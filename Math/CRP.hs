{-# Language GeneralizedNewtypeDeriving #-}

module Math.CRP where

import qualified Data.Vector as V

import Control.Monad
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Random

import Debug.Trace

data PyConfig = PyConfig { alpha :: Double,
                           beta :: Double} deriving Show
data PyRestaurant = PyRestaurant {tables :: V.Vector Int} deriving Show
emptyRestaurant = PyRestaurant (V.fromList [])

newtype PySource m a = PySource {unPySource :: ReaderT PyConfig (StateT PyRestaurant m) a}
    deriving (Monad, 
              MonadState PyRestaurant,
              MonadReader PyConfig,
              MonadFix,
              MonadPlus,
              MonadRandom)

runPySource :: RandomGen g =>
               Double
            -> Double
            -> PySource (Rand g) a
            -> g -> (a, PyRestaurant)
runPySource alpha beta source = 
    let pyConfig = PyConfig alpha beta
    in evalRand $ runStateT (runReaderT (unPySource source) pyConfig) 
       emptyRestaurant

-- ----------------------------------------------------------------------
-- Instance for mtl transformers
instance MonadTrans PySource where
    lift m = PySource $ lift (lift m)
-- ----------------------------------------------------------------------
    

getLogMultinomial :: MonadRandom m => PySource m (V.Vector Double)
getLogMultinomial = do alpha <- asks alpha
                       beta <- asks beta
                       counts <- liftM (V.map fromIntegral) $ gets tables
                       let nCustomers = V.sum counts
                           weights = V.map (log . \x -> x - alpha) counts 
                           newTableWeight = V.fromList [nCustomers * alpha + beta]
                       return (weights V.++ newTableWeight)

cdf :: Num a => V.Vector a -> V.Vector a
cdf xs = V.foldl (\x y -> V.snoc x (V.last x + y)) 
         (V.singleton (V.head xs))
         (V.tail xs)


sampleMultinomial :: MonadRandom m => V.Vector Double -> m Int
sampleMultinomial xs = do r <- getRandom
                          let f i = if (cdf xs V.! i) < r then f (i + 1) else i
                          return $ f 0

testSampleMultinomial = let vals = V.fromList [0.5, 0.4, 0.1]
                         in evalRandIO (sampleMultinomial vals)

sampleCustomer :: MonadRandom m => PySource m Int
sampleCustomer = do log_weights <- getLogMultinomial
                    let weights = V.map exp log_weights
                        z = V.sum weights
                        normalized_weights = V.map (/ z) weights 
                    customer <- sampleMultinomial normalized_weights
                    customers <- gets tables
                    let customers' = updateRestaurant customers customer
                    put $ PyRestaurant customers'
                    return customer
    where updateRestaurant customers customer 
              | customer < V.length customers = let c = customers V.! customer
                                              in customers V.// [(customer, c+1)]
              | otherwise = V.snoc customers 1

data CRP b = CRP {unCRP :: (b, CRP b)}
instance Show (CRP b) where
    show _ = "<undefined crp>"


runCRP crp = let (v, crp') = unCRP crp
             in (v : runCRP crp')

crpMem g0 alpha f = go g0 []
    where go g customers = if newTable then makeNewTableAndGo else chooseOldTableAndGo
                where (r, g') = randomR (0, 1) g    -- get r in (0,1) and next random gen
                      n = fromIntegral $ length customers
                      makeNewTableAndGo =  let b = f g'
                                               (_, g'') = next g'
                                           in b : go g'' (b:customers)
                      chooseOldTableAndGo = let (i, g'') = randomR (0, (length customers) - 1) g'
                                                c = customers !! i
                                            in c : go g'' (c:customers)
                      newTable = r < (alpha / (n + alpha))
                                                 
sample :: (RandomGen g) => g-> Int
sample g = fst $ randomR (0, 1) g 
                                       
               

main = do
  let alpha = 0.2
      beta = 0.5
      source = replicateM 30 sampleCustomer
  g <- getStdGen
  return $ runPySource alpha beta source g








